import torch

from PIL import Image
import os, random

from utils import *
from torchvision.models.detection.faster_rcnn import FasterRCNN

import cv2
import numpy as np
from WiderDataset import WiderDataset
import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='FasterRCNN with MobileNetV2 on WIDER Face Dataset.')
parser.add_argument('--image_scale', type=float, default=1.0, help="Amount by which to downscale dataset images.")
parser.add_argument('--iou', type=float, default=0.6, help="IoU threshold for test.")
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--model_file', type=str, required=True, help="pre-trained model file")
parser.add_argument('--dataset_root', type=str, default=os.path.join('.', 'WIDER'), help='directory path to WIDER dataset root')
parser.add_argument('--threads', type=int, default=4, help='number of threads for data loader to use')
parser.add_argument('--num_images', type=int, default=0, help='Number of images randomly sampled to test against. Omit to test all images in the validation set')
parser.add_argument('--min_box_size', type=int, default=16, help='Minimum box dimension (height, or width) size for ground truth comparison (ignore small ground truth and prediction boxes!)')

opt = parser.parse_args()

imageScale = opt.image_scale
device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')

model = torch.load(opt.model_file, map_location=device)

if type(model) == dict:
    if 'full_model' in model.keys() and model['full_model'] is not None:
        model = model['full_model']
    elif 'model_state' in model.keys() and model['model_state'] is not None:
        if 'backbone_state' in model.keys():
            model = getModifiedFasterRCNN(backbone_state=model['backbone_state'], model_state=model['model_state'])
        else:
            model = getModifiedFasterRCNN(model_state=model['model_state'])

val_dataset = WiderDataset(annotationFile = os.path.join(opt.dataset_root, 'wider_face_split', 'wider_face_val_bbx_gt.txt'),
                        root= os.path.join(opt.dataset_root, 'WIDER_val'), 
                        transforms=getTestTransforms(),
                        imageScale=imageScale,
                        blackListFile=os.path.join(opt.dataset_root, 'blacklist.txt'))

valSampleSize = opt.num_images if opt.num_images else len(val_dataset)

val_sampler = getSubSampler(val_dataset, 1, random.choices(list(range(len(val_dataset))), k=valSampleSize ) )

valLoader = torch.utils.data.DataLoader(val_dataset, 
                                            batch_size=1, 
                                            shuffle=False, 
                                            num_workers=opt.threads,
                                            collate_fn=collate_fn,
                                            sampler=val_sampler)

model = model.to(device)
model.eval()

def filterSizeIdxs(gt, minDimensionSize=16):
    """
    Filter the given bounding box matrix ([N x [x1, y1, x2, y2]]) to get rid of boxes with a dimension (w, or h)
    less than minDimensionSize. 
    Returns the resulting indices
    """
    if minDimensionSize == 0:
        return gt
    # x1 = otherBBoxes[:,0]
    # y1 = otherBBoxes[:,1]
    # x2 = otherBBoxes[:,2]
    # y2 = otherBBoxes[:,3]
    gtWidths = gt[:, 2] - gt[:, 0] # X2 - X1 = width
    gtHeights = gt[:, 3] - gt[:, 1] # Y2 - Y1 = height

    gtValidWidthIdxs = np.where(gtWidths > minDimensionSize)
    gtValidHeightIdxs = np.where(gtHeights > minDimensionSize)

    return np.intersect1d(gtValidWidthIdxs, gtValidHeightIdxs, assume_unique=True)

#Mostly from https://towardsdatascience.com/implementation-of-mean-average-precision-map-with-non-maximum-suppression-f9311eb92522
def calcTruePositives(preds, gt, iouThresh=0.5, minDimensionSize=16):
    """
    For two arrays of shape [N x [x1, y1, x2, y2, confidence, class]]
    We count the number of true positive detections for the preds against the gt (ground truth).

    """
    #Sort by descending confidence
    sortedPredIndices = np.argsort(-preds[:,4])
    preds = preds[sortedPredIndices]

    pred_boxes = preds[:, :4]
    pred_scores = preds[:, 4]
    # pred_labels = preds[:, 5]

    target_boxes = gt[:, :4]

    detected_box_indices = set()

    target_boxes_filtered = target_boxes[filterSizeIdxs(target_boxes, minDimensionSize)]
    pred_idxs_filtered = filterSizeIdxs(pred_boxes, minDimensionSize)
    pred_boxes_filtered = pred_boxes[pred_idxs_filtered]
    pred_scores_filtered = pred_scores[pred_idxs_filtered]

    true_positives = np.zeros(pred_boxes_filtered.shape[0])

    if len(gt):
        for pred_i, pred_box in enumerate(pred_boxes_filtered):
            
            ious = calculateBBoxIoU_fast(pred_box, target_boxes_filtered)
            overlapped_indices = np.where(ious > iouThresh)
            #For every target box that overlapped with this box
            for idx in overlapped_indices[0]:
                #If this box index wasn't already detected by a different predictor
                if idx not in detected_box_indices:
                    true_positives[pred_i] = 1
                    detected_box_indices.add(idx)

    batch_metrics = np.column_stack([true_positives, pred_scores_filtered])

    return batch_metrics, len(target_boxes_filtered)

#From https://github.com/eriklindernoren/PyTorch-YOLOv3/blob/47b7c912877ca69db35b8af3a38d6522681b3bb3/utils/utils.py#L117
def compute_AP(precision, recall):
    """ Compute the average precision, given the recall and precision curves.
    Code originally from https://github.com/rbgirshick/py-faster-rcnn.
    # Arguments
        recall:    The recall curve (list).
        precision: The precision curve (list).
    # Returns
        The average precision as computed in py-faster-rcnn.
    """
    # correct AP calculation
    # first append sentinel values at the end
    mrec = np.concatenate(([0.0], recall, [1.0]))
    mpre = np.concatenate(([0.0], precision, [0.0]))

    # compute the precision envelope
    for i in range(mpre.size - 1, 0, -1):
        mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

    # to calculate area under PR curve, look for points
    # where X axis (recall) changes value
    i = np.where(mrec[1:] != mrec[:-1])[0]

    # and sum (\Delta recall) * prec
    ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap

# Mostly from https://towardsdatascience.com/implementation-of-mean-average-precision-map-with-non-maximum-suppression-f9311eb92522
def calcMetrics(truePositives, num_gt):

    truePositives = truePositives[np.argsort(-truePositives[:,1])]
    tp, conf = truePositives[:,0], truePositives[:, 1]

    fpc = (1 - tp).cumsum()
    tpc = tp.cumsum()

    recall_curve = tpc / (num_gt + 1e-16)
    # r.append(recall_curve[-1])
    
    precision_curve = tpc / (tpc + fpc)

    ap = compute_AP(precision_curve, recall_curve)

    p = precision_curve
    r = recall_curve

    f1_curve = 2 * p * r / (p + r + 1e-16)

    return precision_curve, recall_curve, ap, f1_curve
    
img_num = 0

mAP = 0
for images, targets in valLoader:
    #batch size of 1
    img_num += 1

    images = list(image.to(device) for image in images)
    pred = model(images)
    sortedPreds = sortPredictionsByClass(pred, 1)
    sortedTargets = sortTargetsByClass(targets, 1)


    tp, filtered_targets_len = calcTruePositives(sortedPreds[1], sortedTargets[1], iouThresh=opt.iou, minDimensionSize=opt.min_box_size)
    precision, recall, ap, f1 = calcMetrics(tp, filtered_targets_len)

    print("Image: #{} - {}, mAP: {}".format(img_num, targets[0]['imagePath'], ap))
    mAP += ap

mAP /= img_num
print(f"Average mAP: {mAP}")