
# My FasterRCNN using Transfer Learning for Facial Identification on the WIDER dataset


- [My FasterRCNN using Transfer Learning for Facial Identification on the WIDER dataset](#my-fasterrcnn-using-transfer-learning-for-facial-identification-on-the-wider-dataset)
  - [About](#about)
    - [Trained Model](#trained-model)
    - [Examples](#examples)
  - [Scripts](#scripts)
    - [main.py](#mainpy)
      - [Arguments](#arguments)
      - [Example](#example)
    - [runModel.py](#runmodelpy)
      - [Arguments](#arguments-1)
      - [Example](#example-1)
    - [testAccuracy.py](#testaccuracypy)
      - [Arguments](#arguments-2)
      - [Notes](#notes)
    - [iouThreshold.py](#iouthresholdpy)
      - [Arguments](#arguments-3)
      - [Output](#output)
  - [Model Structure](#model-structure)
    - [Training](#training)
      - [Inputs](#inputs)
      - [Outputs](#outputs)
    - [Testing/Production](#testingproduction)
      - [Inputs](#inputs-1)
      - [Outputs](#outputs-1)
  - [Dataset structure](#dataset-structure)
  - [Tensorboard Integration](#tensorboard-integration)
  - [Future Work Ideas](#future-work-ideas)

## About

Repository with some scripts I used to learn about training a FasterRCNN model on the WIDER dataset to detect and track face positions in images/videos.

I have put in this repository a model that I trained on the WIDER dataset using an RTX2060 for 65 epochs. Not groundbreaking, but the performance is decent.  Given an IoU threshold of 0.6 my trained model achieves an mAP of about 0.5 on the WIDER_val set. I don't reach anywhere near the performance of [Face Detection with Faster R-CNN](http://jianghz.me/pubs/face_det_fg_2017.pdf) but I also didn't take the time to tune my RPN or filter the dataset. Plus, a lot of the faces in the WIDER dataset are very small and very challenging to identify. For my purposes, given that my model performs well on larger examples and runs in real-time on my GPU I'm pretty happy with this project. 

Here's an mAP vs. IoU comparison (produced by running iouThreshold.py):
![mAP vs. IoU graph](iou_thresh.jpeg)

### Trained Model

The model I trained is based on the pretrained [MobileNetV2 backbone](https://pytorch.org/vision/stable/_modules/torchvision/models/mobilenetv2.html). My trained model is in the file in this repository trained_model.tar.gz. It is zipped to try and make the download/upload smaller. The unzipped model file is about 320MB.

### Examples

Here's some examples after running my trained model on an image from WIDER:

Image Test:

![Image test](test_output.jpg)

Code: 

    python runModel.py --model_file trained.pth --input_file test_input.jpg --output_file test_output.jpg

----

Video Test:

<table><tr>
<td> <img src="test_output.gif" alt="Video test 1, Conan" style="width: 250px;"/> </td>
<td> <img src="test_output2.gif" alt="Video test 2, Thor" style="width: 250px;"/> </td>
</tr></table>

Code:

    python runModel.py --model_file trained.pth --input_file test_input.mp4 --output_file test_output.mp4 --in_type v --cuda --score_threshold 0.7

## Scripts

There are a few scripts in the repo, and here's some information on each. Note that my scripts depend on the OpenCV cv2 module:

- main.py
  - Main script for training models on a dataset. Saves the models as .pth files, and is compatible with a checkpoint save/resume system.
- runModel.py
  - Runs a provided .pth model on an input file and produces the resultant output file highlighting the faces in the image (ideally). Also accepts video formats like mp4.
- runPretrained.py
  - A test script for working with a pretrained FasterRCNN model from PyTorch. Pulls an image from the WIDER dataset and highlights found classes.
- testAccuracy.py
  - Runs a model against the WIDER dataset and gives an overall accuracy and statistic scores for the model, based on mean average precision. Sources for algos are linked in the source file. 
- utils.py
  - A collection of useful utility functions that I've found or developed for myself for working with PyTorch and general ML.
- WiderDataset.py
  - A class that inherits torch.utils.data.Dataset and works with the WIDER dataset structure for easily bringing and working with the data.

See below for more information on each script. When writing I was running Python 3.7.6.

I use argparse, so run any of the scripts with `--help` to get a nice description of all the commandline arguments.

Running any of the scripts with `--cuda` will set the device to `cuda:0` rather than `cpu`.

### main.py

This script is used for training a model on the WIDER dataset. The base model is PyTorch's FasterRCNN. This script will create model checkpoints at each epoch, which can be later used to resume training in the event that training must be stopped.

#### Arguments

    usage: main.py [-h] [--image_scale IMAGE_SCALE] [--batchSize BATCHSIZE]
               [--valBatchSize VALBATCHSIZE] [--epochs EPOCHS] [--lr LR]
               [--cuda] [--threads THREADS] [--seed SEED] [--freeze_backbone]
               [--checkpoint_dir CHECKPOINT_DIR]
               [--model_save_dir MODEL_SAVE_DIR] [--dataset_root DATASET_ROOT]
               [--train_num_images TRAIN_NUM_IMAGES]
               [--val_num_images VAL_NUM_IMAGES]
               [--load_checkpoint LOAD_CHECKPOINT]

As it accepts some hyperparameters such as batch size and learning rate, this script has quite a few arguments.

By default, no arguments are required. However, here is some of the more important ones:

- `--image_scale`: float for scaling input images. Scale like 0.5 will result in faster training, but possibly worse performance
- `--batchSize`: Training batch size
- `--valBatchSize`: Batch size for validation
- `--lr`: Learning rate, as a float (like 0.001)
- `--load_checkpoint`: Allows a checkpoint `.pth` file to be loaded. Note these are produced during training and saved into the checkpoint directory specified by `--checkpoint_dir`
- `--model_save_dir`: Directory for final model outputs after training. The final `.pth` file is smaller than the checkpoint files because it has less information about the inner model structure and other information necessary for resuming training.

Note that during training this script will write Tensorboard information. If you want to visualize your training progress, see more information below at [Tensorboard Integration](#tensorboard-integration).

#### Example

    python main.py --image_scale 0.5 --batchSize 5 --cuda --lr 0.001 --epochs 100

To resume from a saved checkpoint, it looks like this:

    python main.py --image_scale 0.5 --batchSize 5 --cuda --lr 0.001 --epochs 100 --load_checkpoint checkpoint_file.pth
  
Note that epoch information is maintained, so it will 'resume' at whatever epoch number was saved into the checkpoint file, and run up to the specified max epochs.

### runModel.py

Use this to run a model file (`.pth`) against an input image or video. Note that my final trained model file is a pickled model object. If you provide your own pickled FasterRCNN model, it will probably work as well.

The input image file/video frame is processed using OpenCV, then turned into a PyTorch Tensor before being passed into the model.

See more about the model inputs/outputs below at [Model Structure](#model-structure).

#### Arguments

    usage: runModel.py [-h] [--cuda] --model_file MODEL_FILE
                  [--score_threshold SCORE_THRESHOLD]
                  [--iou_threshold IOU_THRESHOLD]
                  [--model_input_size MODEL_INPUT_SIZE]
                  [--input_file INPUT_FILE] [--output_file OUTPUT_FILE]
                  [--in_type IN_TYPE] [--out_scaled] [--noshow]

The only required arguments are `--model_file` and `--input_file`.

Note that if you provide an input file that is a video, you must also set `--in_type` to v.

Some important arguments:
- `--score_threshold`: Threshold for the confidence of a predictions. Default 0.6 means only if the model is 60% confident or greater.
- `--iou_threshold`: intersection over union score threshold. 0.5 is pretty good, higher makes it harder to match but means the model is better. 
- `--out_scaled`: The produced output media file will be the same size as the original. The input to the model is still scaled down.
- `--noshow`: Don't show the output as it is being rendered.

#### Example

    python runModel.py trained.pth --input_file test_input.jpg --output_file test_output.jpg

This runs `trained.pth` against `test_input.jpg` and produces `test_output.jpg`.

Just for fun, you can convert an `.mp4` file to a `.gif` file via ffmpeg. Example:

     ffmpeg -t 3 -i test_output.mp4 -vf "fps=10,scale=480:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 test_output.gif

Read more about that at [stackoverflow](https://superuser.com/questions/556029/how-do-i-convert-a-video-to-gif-using-ffmpeg-with-reasonable-quality).

### testAccuracy.py

This script is meant as the 'test' script for a model trained via the `main.py` script. It tests the model against the `WIDER_val` data and gives statistics about the model's accuracy. This script require matplotlib to run.


#### Arguments

    usage: testAccuracy.py [-h] [--image_scale IMAGE_SCALE] [--iou IOU] [--cuda]
                       --model_file MODEL_FILE [--dataset_root DATASET_ROOT]
                       [--threads THREADS] [--num_images NUM_IMAGES]
                       [--min_box_size MIN_BOX_SIZE]
Some important arguments:

- `--model_file`: The only required argument. Path to a trained model `.pth` file.
- `--num_images`: Number of images from the WIDER_val set to test on. Randomly sampled.
- `--min_box_size`: Minimum size for ground truth and prediction boxes. If width or height of a box is below this, we filter it out. Not using this argument means no filtering.

#### Notes

Intersection over Union bounding box comparison considers a bounding box overlapping another bounding box if their 'IoU' value is greather than some threshold. For IoU threshold 0.4 this means that if the predicted box is overlapping the ground truth by at least 40% then it is considered a true positive. If a box overlaps less than 40% it is a false positive, and if there is a ground truth box without an overlapping predicted box then it is a false negative. Everything else would be a true negative.

Read more [here](https://towardsdatascience.com/intersection-over-union-iou-calculation-for-evaluating-an-image-segmentation-model-8b22e2e84686).

This script tests the model against the dataset for the IoU threshold 0.3 - 1.0 and records the mAP (mean average precision) for each IoU value, which is charted as mAP over IoU after the tests are complete.

Read more about mAP [here](https://towardsdatascience.com/implementation-of-mean-average-precision-map-with-non-maximum-suppression-f9311eb92522).

### iouThreshold.py

This script is similar to `testAccuracy.py` but it tests the model against the `WIDER_val` set for a range of IoU values and then charts mAP vs. IoU. This is a good way to figure out an ideal IoU value for your trained model (look for the curve's inflection point).


#### Arguments

    usage: iouThreshold.py [-h] [--image_scale IMAGE_SCALE] [--cuda] --model_file
                       MODEL_FILE [--dataset_root DATASET_ROOT]
                       [--threads THREADS] [--num_images NUM_IMAGES]
                       [--min_gt_size MIN_GT_SIZE]


Some important arguments:

- `--model_file`: The only required argument. Path to a trained model `.pth` file.
- `--num_images`: Number of images from the WIDER_val set to test on. Randomly sampled.
- `--min_box_size`: Minimum size for ground truth and prediction boxes. If width or height of a box is below this, we filter it out. Not using this argument means no filtering.

#### Output

This script gives a chart of mAP vs. IoU. These are my results:

![mAP vs. IoU graph](iou_thresh.jpeg)


## Model Structure

This repository is using the FasterRCNN model from torchvision.models.detection.faster_rcnn and using transfer learning to train it against the WIDER dataset. Since that's the case, PyTorch's documentation on FasterRCNN is valid for this repository, with some small modification of my own. Check out their information about the model [here](https://pytorch.org/vision/stable/models.html#id57). 

You can see the code in `utils.py` for how the model is structured:

```python
def getModifiedFasterRCNN(min_size=224, imageScale=0.5, frozenBackbone=False, backbone_state=None, model_state=None):
  backbone = getMobileNetV2Backbone(frozen=frozenBackbone, backbone_state=backbone_state)

  #RPN for 5 x 3 anchors per spatial location (5 different sizes, 3 different spatial ratios)
  sizes = [16, 32, 64, 128, 256]
  sizes = [int(size * imageScale) for size in sizes]

  anchor_generator = AnchorGenerator(sizes=(sizes,), 
                                              aspect_ratios=((0.65, 1.0, 1.5),))

  #Region of Interest cropping, and crop size after rescaling
  roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'], 
                                                  output_size=7,
                                                  sampling_ratio=2)

  model = FasterRCNN(backbone, num_classes=2, rpn_anchor_generator=anchor_generator,
                      box_roi_pool=roi_pooler, min_size=min_size)

  if model_state is not None:
      model.load_state_dict(model_state)

  return model
```

As you can see, it's using 5 x 3 anchors for the RPN. I adjusted the aspect ratios to more closely approximate the shape of a face, rather than the example I've seen elsewhere of ratios of 0.5, 1, and 2 for broader object detection. It may be worth modifying the model structure here if you want to get better results.

Below is some information on how the script input data is structured (primarily my `.pth` files) for this project.

### Training

If you're training a new model (not from a checkpoint file) then the script grabs a pretrained model from torchvision.models.detection.fasterrcnn_resnet50_fpn and freezes the gradients in the entire model (this is a transfer learning project, after all). A new FastRCNNPredictor from torchvision.models.detection.faster_rcnn is used to replace the pretrained classifier with a new one, with only 2 classes (0: background, 1: face).

#### Inputs
When training a new model via `main.py`, the inputs just come from the WIDER dataset, prepared via the WiderDataset class. 

If you're resuming from a checkpiont, then the checkpoint `.pth` file is loaded using torch.load. The checkpoint is represented as a python dictionary with the following keys:
- `'full_model'`: The full Pytorch model binary for the classifier.
- `'model_state_dict'`: The state_dict() from the model.
- `'optimizer_state_dict'`: The active optimizer's (torch.optim.AdamW) state_dict().
- `'scheduler_state_dict'`: The active scheduler's (torch.optim.lr_scheduler.StepLR) state_dict().
- `'epoch'`: epoch number.
- `'loss'`: Last loss value.

#### Outputs

The checkpoint files are written out given the structure above after each epoch. 

When the model is finished training, then the final model is saved directly via `torch.save()`. While it's potentially possible to save disk space by just saving the model's weights, it's harder to use this later because the entire model must be reconstructed exactly before applying the weights. This way, the model can just be loaded and run.

### Testing/Production
When running/testing the model via `runModel.py` or `testAccuracy.py` one can run it with the final output `.pth` file or a checkpoint file. 

#### Inputs
The model inputs here are collected from the given `.pth` file. These files are explained more above at Training->Inputs.

#### Outputs
`runModel.py` produces an image or video with boxes placed in the media that represent face predictions. `testAccuracy.py` produces a chart showing mAP over IoU threshold.


## Dataset structure

If training using the main.py script, a path to the WIDER dataset root directory must be provided. The dataset can be downloaded from [this page](http://shuoyang1213.me/WIDERFACE/).

The structure of this dataset directory system once downloaded and extracted should be put together as follows:

    WIDER
    |  \wider_face_split
    |  |  \misc .mat and .txt (annotation) files
    |  \WIDER_test
    |  |  \images
    |  |  |  \0--Parade
    |  |  |  \ ...
    |  |  |  \ ...
    |  \WIDER_train
    |  |  \ same as test
    |  \WIDER_val
    |  |  \ same as test and train
    |  blacklist.txt

The blacklist.txt is my own addition, to pull out problematic image files that were causing issues with my training. The format for this is just a file with an image name on each line.

The `.txt` files under `wider_face_split` define the AABB boxes that describe the face ground truths for each image.

The dataset management is all handled by the `WiderDataset.py` script contents. It uses `cv2` and `PIL` to prepare the images for use.

## Tensorboard Integration

The main.py script uses SummaryWriter from torch.utils.tensorboard. It will output training information to the default output directory `runs`.

Example training visual:

![Training on tensorboard](tensorboard.jpg)

## Future Work Ideas

- I've also worked some with the EfficientNet models, so it may be worth trying that rather than the MobileNetV2. I may also try MobileNetV3-Large and see how it performs.
- Using this model to feed into a facial recognition model. It seems like it would be fairly easy to use the faces grabbed from each frame to then train/test a facial recognition model.
- Try other type of object recognition training on other datasets, such as COCO.