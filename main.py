import numpy as np
import torchvision
import torch
from torch import nn
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor

from WiderDataset import WiderDataset
from torchvision import transforms

from PIL import Image
import math, time, copy, os, random
import cv2
import matplotlib.pyplot as plt

from utils import *

from torch.utils.tensorboard import SummaryWriter 

import argparse

parser = argparse.ArgumentParser(description='Training script for FasterRCNN with MobileNetV2 on WIDER Face Dataset.')
parser.add_argument('--image_scale', type=float, default=1.0, help="Amount by which to downscale dataset images.")
parser.add_argument('--batchSize', type=int, default=5, help='training batch size')
parser.add_argument('--valBatchSize', type=int, default=10, help='validation batch size')
parser.add_argument('--epochs', type=int, default=10, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.01, help='Learning Rate. Default=0.01')
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--threads', type=int, default=4, help='number of threads for data loader to use')
parser.add_argument('--seed', type=int, default=345, help='random seed to use. Default=123')
parser.add_argument('--freeze_backbone', action='store_true', help="Freeze backbone gradients?")
parser.add_argument('--checkpoint_dir', type=str, default=os.path.join('.', 'checkpoints'), help="directory to save checkpoints into")
parser.add_argument('--model_save_dir', type=str, default=os.path.join('.', 'saved_models'), help="directory to save final models into")
parser.add_argument('--dataset_root', type=str, default=os.path.join('.', 'WIDER'), help='directory path to WIDER dataset root')

parser.add_argument('--train_num_images', type=int, default=-1, help='Number of training images to use from the set. Default is to use the whole dataset.')
parser.add_argument('--val_num_images', type=int, default=-1, help='Number of validation images to use from dataset. Default is to use the whole dataset.')

parser.add_argument('--load_checkpoint', type=str, help="model file to load if resuming training")

opt = parser.parse_args()

def getTrainingTransforms():
    return transforms.Compose([
        transforms.ColorJitter(brightness=0.3, contrast=0.1, saturation=0.1, hue=0.1),
        # transforms.RandomResizedCrop(size=512),
        #transforms.RandomRotation(degrees=15),
        #transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean_sums, std_sums)
    ])

def getValidationTransforms():
    return transforms.Compose([
        # transforms.Resize(512),
        #transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean_sums, std_sums)
    ])

    
def getPretrainedFasterRCNN(imageScale=1.0):
    # load a model pre-trained pre-trained on COCO
    model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)

    for param in model.parameters():
        param.requires_grad = False

    # replace the classifier with a new one, that has
    # num_classes which is user-defined
    num_classes = 2  # 1 class (person) + background
    # get number of input features for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    # replace the pre-trained head with a new one
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)

    return model

def train_model(model, dataloaders, optimizer, scheduler, device, num_epochs=25, checkpoint_dir=None, start_epoch=0):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    writer = SummaryWriter()

    for epoch in range(start_epoch, num_epochs + start_epoch):
        epoch_since = time.time()
        print('Epoch {}/{}'.format(epoch, num_epochs - 1 + start_epoch))
        print('-' * 10)

        writer.add_scalar('memory/allocated', torch.cuda.memory_allocated(), epoch)
        writer.add_scalar('memory/cached', torch.cuda.memory_cached(), epoch)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            epoch_loss = 0
            iterations = 0
            batchNum = 0
            for images, targets in dataloaders[phase]:
                #print('loop', phase)
                images = list(image.to(device) for image in images)
                targets = [{k: v.to(device) for k, v in t.items()} for t in targets]

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    if phase == 'val':
                        model.train()

                    loss_dict = model(images, targets)

                    losses = sum(loss for loss in loss_dict.values())
                    print('Batch {}/{}'.format(batchNum, len(dataloaders[phase]) - 1))
                    batchNum += 1

                    if not math.isfinite(losses.item()):
                        print("Loss is {} during {}".format(losses.item(), phase))
                        if phase == 'train':
                            print('Quitting!')
                            quit()

                    if phase == 'val':
                        model.eval()
                        #preds = model(images)
                        #print('targets:', targets)
                        #print(preds)
                        # for pred in preds:
                        #     print(pred['boxes'].size())

                    #losses = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        # zero the parameter gradients
                        optimizer.zero_grad()
                        losses.backward()
                        optimizer.step()

                    if math.isfinite(losses.item()):
                        epoch_loss += losses.item()
                    else:
                        epoch_loss += 1

            lossloss = epoch_loss / len(dataloaders[phase])
            if phase == 'train' and scheduler:
                scheduler.step()

            print('{} Loss: {:.4f}'.format(phase, lossloss))

            writer.add_scalar('loss/{}'.format(phase), lossloss, epoch)

            # deep copy the model
            # if phase == 'val' and epoch_acc > best_acc:
            #     best_model_wts = copy.deepcopy(model.state_dict())
            if checkpoint_dir is not None and (epoch + 1) % 5 == 0 and phase == 'val':
                print('Checkpint saving for epoch {}.'.format(epoch))
                saveModelCheckpoint(epoch, 
                                    epoch_loss, 
                                    model, 
                                    optimizer, 
                                    scheduler, 
                                    os.path.join(checkpoint_dir, 'model_epoch_{}.pth'.format(epoch)))
        time_elapsed = time.time() - epoch_since
        print('Epoch completed in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

    print()
    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

    writer.flush()
    writer.close()

    # load best model weights
    # model.load_state_dict(best_model_wts)
    return model


os.makedirs(opt.model_save_dir, exist_ok=True)
checkpoint_dir = os.path.join(opt.checkpoint_dir, 'run{}'.format(getTimestamp()))
os.makedirs(checkpoint_dir, exist_ok=True)

#Make RNG deterministic
torch.manual_seed(opt.seed)
np.random.seed(opt.seed)

device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')
imageScale = opt.image_scale
lr = opt.lr
num_epochs=opt.epochs

train_dataset = WiderDataset(annotationFile = os.path.join(opt.dataset_root, 'wider_face_split', 'wider_face_train_bbx_gt.txt'),
                        root= os.path.join(opt.dataset_root, 'WIDER_train'),
                        transforms=getTrainingTransforms(),
                        imageScale=imageScale,
                        blackListFile=os.path.join(opt.dataset_root, 'blacklist.txt'))
                        
val_dataset = WiderDataset(annotationFile = os.path.join(opt.dataset_root, 'wider_face_split', 'wider_face_val_bbx_gt.txt'),
                        root= os.path.join(opt.dataset_root, 'WIDER_val'), 
                        transforms=getValidationTransforms(),
                        imageScale=imageScale,
                        blackListFile=os.path.join(opt.dataset_root, 'blacklist.txt'))

trainSampleSize = opt.train_num_images if opt.train_num_images > 0 else len(train_dataset)
valSampleSize = opt.val_num_images if opt.val_num_images > 0 else len(val_dataset)

train_sampler = getSubSampler(train_dataset, 1, random.choices(list(range(len(train_dataset))), k=trainSampleSize ) )
val_sampler = getSubSampler(val_dataset, 1, random.choices(list(range(len(val_dataset))), k=valSampleSize ) )

trainLoader = torch.utils.data.DataLoader(train_dataset, 
                                            batch_size=opt.batchSize, 
                                            shuffle=False, 
                                            num_workers=opt.threads,
                                            collate_fn=collate_fn,
                                            sampler=train_sampler)


valLoader = torch.utils.data.DataLoader(val_dataset, 
                                            batch_size=opt.valBatchSize, 
                                            shuffle=False, 
                                            num_workers=opt.threads,
                                            collate_fn=collate_fn,
                                            sampler=val_sampler)


dataloaders = { 
    'train': trainLoader,
    'val': valLoader
}

startEpoch = 0

print()
if opt.load_checkpoint is not None:

    print('Loading model from previously saved checkpoint', opt.load_checkpoint)

    checkpointDict = loadModelCheckpointDict(opt.load_checkpoint, device)

    trainableModel = checkpointDict['full_model']

    params = [p for p in trainableModel.parameters() if p.requires_grad]

    optimizer = torch.optim.AdamW(params, lr=lr )

    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.5)

    startEpoch, _ = loadModelFromCheckpoint(checkpointDict, trainableModel, optimizer, lr_scheduler, loadFullModel=True)
    trainableModel.to(device)
    print('Starting at epoch', startEpoch)

else:
    print('Creating new model template for FasterRCNN')
    trainableModel = getModifiedFasterRCNN(imageScale=imageScale, frozenBackbone=opt.freeze_backbone)
    trainableModel = trainableModel.to(device)

    params = [p for p in trainableModel.parameters() if p.requires_grad]
    #optimizer = torch.optim.SGD(params, lr=lr, momentum=0.9, weight_decay=0.0005)

    optimizer = torch.optim.AdamW(params, lr=lr )

    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=8, gamma=0.1)
    #lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer)

print('Run Parameters:')
print('\tImage Sscale:', imageScale)
print('\tTrain Batch Size:', opt.batchSize)
print('\tVal Batch Size:', opt.valBatchSize)
print('\tEpochs:', opt.epochs)
print('\tLR:', opt.lr)
print('\tDevice:', device)
print('\tThreads:', opt.threads)
print('\tFreeze Backbone:', opt.freeze_backbone)
print('\tNum Training Images:', opt.train_num_images)
print('\tNum Val Images:', opt.val_num_images)
print()

best_model = train_model(trainableModel, dataloaders, optimizer, lr_scheduler, device, num_epochs, checkpoint_dir=checkpoint_dir, start_epoch=startEpoch)

saveModelCheckpoint(num_epochs + startEpoch - 1, 
                    0, 
                    trainableModel, 
                    optimizer, 
                    lr_scheduler, 
                    os.path.join(checkpoint_dir, 'model_final.pth'))

saveFileName = 'final_model_fasterrcnn_wider{}.pth'.format(getTimestamp())
saveFilePath = os.path.join(opt.model_save_dir, saveFileName)
torch.save({'model_state' : best_model.state_dict(), 'backbone_state': best_model.backbone.state_dict()}, saveFilePath)
print('Model file saved to {}'.format(saveFilePath))