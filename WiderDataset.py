import os
import numpy as np
import torch
from PIL import Image
import cv2

def getBlackList(blacklistFile):
    fileLines = []
    with open(blacklistFile) as bF:
        fileLines = [line.rstrip() for line in bF]

    return set(fileLines)

def parseAnnotations(annotationFile):
    fileInfos = {}

    fileLines = []
    with open(annotationFile) as aF:
        fileLines = [line.rstrip() for line in aF]

    lineIdx = 0
    while lineIdx < len(fileLines):
        fileDict = {}
        imagePath = str(fileLines[lineIdx])
        lineIdx += 1
        fileDict['numFaces'] = int(fileLines[lineIdx])
        lineIdx += 1

        badEntry = False
        if (fileDict['numFaces'] == 0):
            badEntry = True

        facesList = []
        for faceIdx in range(fileDict['numFaces']):
            faceLine = fileLines[lineIdx].split(' ')
            faceDetails = [int(detail) for detail in faceLine]
            faceDict = {}
            #BBox is in X, Y, W, H format
            faceDict['bbox'] = faceDetails[0:4]
            
            bbox = faceDict['bbox']
            if bbox[0] == 0 and bbox[1] == 0 and bbox[2] == 0 and bbox[3] == 0:
                badEntry = True
                break

            faceDict['blur'] = faceDetails[4]
            faceDict['expression'] = faceDetails[5]
            faceDict['illumination'] = faceDetails[6]
            faceDict['invalid'] = faceDetails[7]
            faceDict['occlusion'] = faceDetails[8]
            faceDict['pose'] = faceDetails[9]
            facesList.append(faceDict)
            lineIdx += 1

        #For some reason, even if the number of faces is listed as 0 there's an entry 
        # with all 0's that we need to skip
        if (badEntry):
            fileDict['numFaces'] = 0
            lineIdx += 1
            fileInfos[imagePath] = fileDict
            continue

        fileDict['faces'] = facesList
        fileInfos[imagePath] = fileDict

    return fileInfos

#Root should end in WIDER_train, WIDER_val, or WIDER_test
def findFilePaths(root, blackListFile=None):
    basePath = os.path.join(root, 'images')
    categoryFolders = list(sorted(os.listdir(basePath)))

    imagePaths = []
    for categoryFolder in categoryFolders:
        categoryFolder_files = set((os.listdir(os.path.join(basePath, categoryFolder))))

        if blackListFile is not None:
            blacklist = getBlackList(blackListFile)
            foundMatches = categoryFolder_files & blacklist
            # if len(foundMatches) > 0:
            #     print(foundMatches)
            categoryFolder_files = categoryFolder_files - foundMatches #Remove files that were matched with blacklist

        categoryFolder_paths = [str(os.path.join(categoryFolder, categoryFile)) for categoryFile in categoryFolder_files]
        imagePaths += sorted(categoryFolder_paths)

    return imagePaths

class WiderDataset(torch.utils.data.Dataset):
    def __init__(self, annotationFile, root, transforms=None, imageScale=1.0, blackListFile=None):
        self.root = root
        self.annotationFile = annotationFile
        self.transforms = transforms

        self.imagePaths = findFilePaths(root, blackListFile)
        self.imageScale = imageScale

        if annotationFile is not None:
            self.annotationData = parseAnnotations(annotationFile)
        else:
            self.annotationData = None

    def __getitem__(self, idx):
        try:
            filePath = self.imagePaths[idx]
        except Exception as error:
            print('Exception at', idx, error)
            quit()

        imagePath = os.path.join(self.root, 'images', filePath)

        #Load image
        img = Image.open(imagePath).convert("RGB")
        originalImgWidth, originalImgHeight = img.size
        #print(originalImgWidth, originalImgHeight)

        if self.transforms is not None:
            img = cv2.resize(np.array(img), (int(originalImgWidth * self.imageScale), int(originalImgHeight * self.imageScale)))
            img = Image.fromarray(img)
            img = self.transforms(img)

        if torch.isnan(img).any():
            print('Image {} has nan value after transform.'.format(imagePath))

        if (self.annotationData is None):
            return img, None

        resizedImgWidth, resizedImgHeight = img.size()[2], img.size()[1]
        #print(resizedImgWidth,resizedImgHeight)

        imageData = self.annotationData[self.imagePaths[idx]]

        num_faces = imageData['numFaces']
        if num_faces == 0:
            return self.__getitem__(idx + 1)

        faces = imageData['faces']
        boxes = []

        for face in faces:
            faceBox = face['bbox']
            #print('Original face bbox:', faceBox)
            xmin = faceBox[0] / originalImgWidth * resizedImgWidth
            ymin = faceBox[1] / originalImgHeight * resizedImgHeight
            xmax = xmin + (faceBox[2] / originalImgWidth * resizedImgWidth)
            ymax = ymin + (faceBox[3] / originalImgHeight * resizedImgHeight)
            t = [xmin, ymin, xmax, ymax]
            boxes.append(t)

        #FasterRCNN expects Float32 for box bounds
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        #print(num_faces)
        #print(boxes)
        #In WIDER there's only one class label. FasterRCNN expects labels as Int64
        labels = torch.ones((num_faces,), dtype=torch.int64)

        #FasterRCNN expects a target dict
        target = {}
        target['imagePath'] = self.imagePaths[idx]
        target['boxes'] = boxes
        target['labels'] = labels

        return img, target

    def __len__(self):
        return len(self.imagePaths)