import cv2 
import matplotlib.pyplot as plt
import torch
import torchvision
import numpy as np
from torch.utils.data import SubsetRandomSampler
from torch import nn
from torchvision import transforms
from PIL import Image
from datetime import datetime

from torchvision.models.detection.faster_rcnn import FasterRCNN
from torchvision.models.detection.rpn import AnchorGenerator

from collections import OrderedDict

mean_sums = [0.485, 0.456, 0.406]
std_sums = [0.229, 0.224, 0.225]

def getTestTransforms():
    return transforms.Compose([
        # transforms.Resize(512),
        #transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean_sums, std_sums)
    ])


def renameStateDictParameters(old_dict, key_transform):
    new_dict = OrderedDict()
    for key, value in old_dict.items():
        new_key = key_transform(key)
        new_dict[new_key] = value

    return new_dict

def getMobileNetV2Backbone(frozen=True, backbone_state=None):
    backbone_model = torchvision.models.mobilenet_v2(pretrained=True)

    if frozen:
        for param in backbone_model.parameters():
            param.requires_grad = False

        backbone_model.classifier = nn.Sequential(
            nn.Dropout(p=0.2, inplace=False),
            nn.Linear(1280, 1000, bias=True)
        )

    backbone = backbone_model.features
    #MobileNet_V2 has 1280 output channels
    backbone.out_channels = 1280

    if backbone_state is not None:
        backbone.load_state_dict(backbone_state, strict=False)

    return backbone

#Pulled from https://pytorch.org/tutorials/intermediate/torchvision_tutorial.html
#situation 2 (replacing backbone)
def getModifiedFasterRCNN(min_size=224, imageScale=0.5, frozenBackbone=False, backbone_state=None, model_state=None):
    if backbone_state is None and model_state is not None:
        def k_trans(key):
            if key.startswith('backbone.'):
                return key.replace('backbone.', '')
            else:
                return key
        #Backbone state is stored within full model_state, no need to load it separately if we can rename the params
        backbone_state = renameStateDictParameters(model_state, k_trans)

    backbone = getMobileNetV2Backbone(frozen=frozenBackbone, backbone_state=backbone_state)

    #RPN for 5 x 3 anchors per spatial location (5 different sizes, 3 different spatial ratios)
    sizes = [16, 32, 64, 128, 256]
    sizes = [int(size * imageScale) for size in sizes]

    anchor_generator = AnchorGenerator(sizes=(sizes,), 
                                                aspect_ratios=((0.65, 1.0, 1.5),))

    #Region of Interest cropping, and crop size after rescaling
    roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'], 
                                                    output_size=7,
                                                    sampling_ratio=2)

    model = FasterRCNN(backbone, num_classes=2, rpn_anchor_generator=anchor_generator,
                        box_roi_pool=roi_pooler, min_size=min_size)

    if model_state is not None:
        model.load_state_dict(model_state)

    return model

def showImage(imageTensor, boxes=[]):
    #img = transforms.ToPILImage()(imageTensor)
    img = imageTensor.mul(255).permute(1, 2, 0).byte().numpy()
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    for i in range(len(boxes)):
        start_coords = (boxes[i][0], boxes[i][1])
        end_coords = (boxes[i][2], boxes[i][3])
        cv2.rectangle(img, start_coords, end_coords, color=(0, 255, 0), thickness=3)

        # plt.figure(figsize=(20, 30))
    plt.imshow(img)
    plt.xticks([])
    plt.yticks([])
    plt.show()

def collate_fn(batch):
    return tuple(zip(*batch))

def saveModelState(model, path):
    torch.save(model.state_dict(), path)

def loadModelState(model, path, device='cpu'):
    model.load_state_dict(torch.load(path, map_location=device))

def saveModelCheckpoint(epoch, loss, model, optimizer, scheduler, path):
    torch.save({
        'epoch': epoch,
        'full_model': model,
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optimizer.state_dict(),
        'scheduler_state_dict': scheduler.state_dict(),
        'loss': loss
    }, path)

def loadModelCheckpointDict(path, device='cpu'):
    return torch.load(path, map_location=device)

def loadModelFromCheckpoint(checkpointDict, model, optimizer, scheduler, loadFullModel=False):
    checkpoint = checkpointDict

    if loadFullModel:
        model = checkpoint['full_model']
    else:
        model.load_state_dict(checkpoint['model_state_dict'])

    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    scheduler.load_state_dict(checkpoint['scheduler_state_dict'])

    return checkpoint['epoch'], checkpoint['loss']

def getSubSampler(dataset, percentage, indices):
    np.random.shuffle(indices)

    numIndices = int(len(indices) * percentage)

    return SubsetRandomSampler( indices[0:numIndices] )

def showPredictions(img, resultList):
    img.convert('RGB')
    img = np.array(img)
    # img = img[:, :, ::-1].copy()

    boxes = resultList['boxes']
    scores = resultList['scores']
    labels = resultList['labels']

    for i in range(len(boxes)):
        start_coords = (boxes[i][0], boxes[i][1])
        end_coords = (boxes[i][2], boxes[i][3])
        cv2.rectangle(img, start_coords, end_coords, color=(0, 255, 0), thickness=2)
        cv2.putText(img, 
                    '{:1.3f}'.format(scores[i]),
                    (boxes[i][0], boxes[i][1]),
                    cv2.FONT_HERSHEY_SIMPLEX, 
                    fontScale=1, color=(0, 255, 0), thickness=2)

        print('Box:', boxes[i], '\t\tScore:', scores[i], '\tClass:', labels[i])
        # plt.figure(figsize=(20, 30))

    plt.imshow(img)
    plt.xticks([])
    plt.yticks([])
    plt.show()


def showPredictionsNumpy(img, resultList):
    img.convert('RGB')
    img = np.array(img)
    # img = img[:, :, ::-1].copy()

    for i in range(len(resultList)):
        currBox = resultList[i]
        start_coords = (int(round(currBox[0])), int(round(currBox[1])))
        end_coords = (int(round(currBox[2])), int(round(currBox[3])))
        cv2.rectangle(img, start_coords, end_coords, color=(0, 255, 0), thickness=2)
        cv2.putText(img, 
                    '{:1.3f}'.format(currBox[4]),
                    (int(round(currBox[0])), int(round(currBox[1]))),
                    cv2.FONT_HERSHEY_SIMPLEX, 
                    fontScale=0.5, color=(0, 255, 0), thickness=2)

        print('Box:', currBox[0:4], '\t\tScore:', currBox[4], '\tClass:', currBox[5])
        # plt.figure(figsize=(20, 30))

    plt.imshow(img)
    plt.xticks([])
    plt.yticks([])
    plt.show()

def getModelPredictions(images, model, device, scoreThreshold):
    model = model.to(device)
    model.eval()
    modelInput = [getTestTransforms()(image).to(device) for image in images]

    pred = model(modelInput)

    imageResults = []
    for prediction in pred:
        res = {}
        res['boxes'] = np.array([[box[0], box[1], box[2], box[3]] for box in prediction['boxes'].detach().cpu().numpy()])
        res['scores'] = np.array([score for score in prediction['scores'].detach().cpu().numpy()])
        res['labels'] = np.array([cl for cl in prediction['labels'].detach().cpu().numpy()])

        threshIndices = np.where(res['scores'] > scoreThreshold)

        res['boxes'] = res['boxes'][threshIndices]
        res['scores'] = res['scores'][threshIndices]
        res['labels'] = res['labels'][threshIndices]

        imageResults.append(res)

    return imageResults

#BBox format of [x1, y1, x2, y2]
def calculateBBoxIoU(bbox1, bbox2):    
    if (bbox1[0] >= bbox1[2]) or (bbox1[1] >= bbox1[3]):
        raise AssertionError('BBOX1 is not valid.')
    if (bbox2[0] >= bbox2[2]) or (bbox2[1] >= bbox2[3]):   
        raise AssertionError('BBOX2 is not valid.')

    # If bottom-right corner of bbox 1 is less than or above the top-left 
    # corner of bbox 2.
    if (bbox1[2] < bbox2[0]) or (bbox1[3] < bbox2[1]):
        return 0.0 

    # if top-left corner of bbox 1 is greater than or below bottom-right
    # corner of bbox 2
    if (bbox1[0] > bbox2[2]) or (bbox1[1] > bbox2[3]):
        return 0.0

    bbox1_area = (bbox1[2] - bbox1[0]) * (bbox1[3] - bbox1[1])
    bbox2_area = (bbox2[2] - bbox2[0]) * (bbox2[3] - bbox2[1])

    #Calculate intersection, which is a box made up of max(top left corners) and
    # min(bottom right corners)
    int_tl = [ max(bbox1[0], bbox2[0]), max(bbox1[1], bbox2[1]) ]
    int_br = [ min(bbox1[2], bbox2[2]), min(bbox1[3], bbox2[3]) ]

    int_area = ( int_br[0] - int_tl[0] ) * ( int_br[1] - int_tl[1] )

    #Calculate union area
    union_area = (bbox1_area + bbox2_area) - int_area

    #Result is ratio of intersection/union

    return int_area / union_area

def calculateBBoxIoU_fast(bbox1, otherBBoxes):
    
    x1 = otherBBoxes[:,0]
    y1 = otherBBoxes[:,1]
    x2 = otherBBoxes[:,2]
    y2 = otherBBoxes[:,3]

    #Compute bounding box areas and sort the boxes by bottom-right y-coordinate of 
    # the bounding box
    area1 = (bbox1[2] - bbox1[0] + 1) * (bbox1[3] - bbox1[1] + 1)
    areaOther = (x2 - x1 + 1) * (y2 - y1 + 1)

    # find the largest (x, y) coordinates for the start of
    # the bounding box and the smallest (x, y) coordinates
    # for the end of the bounding box
    xx1 = np.maximum(bbox1[0], x1)
    yy1 = np.maximum(bbox1[1], y1)
    xx2 = np.minimum(bbox1[2], x2)
    yy2 = np.minimum(bbox1[3], y2)

    # compute the width and height of the bounding box
    w = np.maximum(0, xx2 - xx1 + 1)
    h = np.maximum(0, yy2 - yy1 + 1)

    int_area = w * h
    union_area = (area1 + areaOther) - int_area

    # compute the ratio of overlap
    iou = int_area / union_area

    return iou


def sortPredictionsByClass(predictions, num_classes):
    classDict = {}

    combinedArray = np.zeros((0, 6))
    for pred in predictions:
        boxes = pred['boxes'].detach().cpu().numpy()
        scores = pred['scores'].detach().cpu().numpy()
        labels = pred['labels'].detach().cpu().numpy()

        combinedArray = np.row_stack([combinedArray, np.column_stack([boxes, scores, labels])])

    for c in range(1, num_classes + 1):
        classIndices = np.where(combinedArray[:, 5] == c)
        classDict[c] = combinedArray[classIndices]

    return classDict

def sortTargetsByClass(predictions, num_classes):
    classDict = {}

    combinedArray = np.zeros((0, 5))
    for pred in predictions:
        boxes = pred['boxes'].detach().cpu().numpy()
        labels = pred['labels'].detach().cpu().numpy()

        combinedArray = np.row_stack([combinedArray, np.column_stack([boxes, labels])])

    for c in range(1, num_classes + 1):
        classIndices = np.where(combinedArray[:, 4] == c)
        classDict[c] = combinedArray[classIndices]

    return classDict

def nonMaximumSuppression_slow(classPredictions, confThresh = 0.2, iouThresh=0.4):
    if len(classPredictions) == 0:
        return []

    # Eliminate predictions with a score less than confThresh
    predIndices = np.where(classPredictions[:, 4] > confThresh)

    allIndexSet = set(predIndices[0])
    suppressedIndices = set()

    for predictionIdx in allIndexSet:
        if predictionIdx in suppressedIndices:
            continue
        for otherIdx in allIndexSet ^ suppressedIndices:
            if otherIdx == predictionIdx:
                continue
            iou = calculateBBoxIoU(classPredictions[predictionIdx, 0:4], classPredictions[otherIdx, 0:4])
            print('Prediction Idx:', predictionIdx)
            print('Other Idx:', otherIdx)
            print(iou)
            if iou > iouThresh:
                print('Suppressing', otherIdx)
                suppressedIndices.add(otherIdx)
            print()

    return classPredictions[list(allIndexSet ^ suppressedIndices)]


# From https://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/
def nonMaximumSuppression_fast(boxes, confThresh = 0.5, iouThresh=0.4):
    """
    Expects classPredictions as a numpy array that is [N x [box[0:4], confidence, class]]

    """

    #If no boxes, don't run algorithm
    if len(boxes) == 0:
        return []

    # Make sure it's a float array 
    if boxes.dtype.kind == 'i':
        boxes.astype('float')

    #Picked indices list
    pick = []

    #Filter indices by confidence score threshold (My added)
    boxes = boxes[np.where(boxes[:, 4] > confThresh)]

    x1 = boxes[:,0]
    y1 = boxes[:,1]
    x2 = boxes[:,2]
    y2 = boxes[:,3]

    #Compute bounding box areas and sort the boxes by bottom-right y-coordinate of 
    # the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)

    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]

        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
            np.where(overlap > iouThresh)[0])))

    return boxes[pick]

def getTimestamp():
    t = datetime.now()
    return "{}{}{}_{}{}{}".format(t.year, t.month, t.day, t.hour, t.minute, t.second)


def resizeImage(image, imageScale):
    originalImgWidth, originalImgHeight = image.size

    image = cv2.resize(np.array(image), (int(originalImgWidth * imageScale), int(originalImgHeight * imageScale)))
    image = Image.fromarray(image)

    return image

def runModelTest(image, model, imageScale, device, confThreshold=0.5, iouThreshold=0.4):
    finalImage = resizeImage(image, imageScale)

    #results = getModelPredictions([finalImage], model, device, threshold)

    model = model.to(device)
    model.eval()
    modelInput = [getTestTransforms()(finalImage).to(device)]
    pred = model(modelInput)
    sortedDict = sortPredictionsByClass(pred, 1)

    boxes = np.ndarray((0, 6))
    for k in sortedDict.keys():
        boxes = np.row_stack([boxes, nonMaximumSuppression_fast(sortedDict[k], confThresh=confThreshold, iouThresh=iouThreshold)])

    showPredictionsNumpy(finalImage, boxes)

def cvPutBoxes(img, boxes, upscale=1.0):
    for i in range(len(boxes)):
        currBox = boxes[i]
        start_coords = (int(round(currBox[0] * upscale)), int(round(currBox[1] * upscale)))
        end_coords = (int(round(currBox[2] * upscale)), int(round(currBox[3] * upscale)))
        cv2.rectangle(img, start_coords, end_coords, color=(0, 255, 0), thickness=2)
        cv2.putText(img, 
                    '{:1.3f}'.format(currBox[4]),
                    start_coords,
                    cv2.FONT_HERSHEY_SIMPLEX, 
                    fontScale=0.5 * upscale, color=(0, 255, 0), thickness=int(round(2 * upscale)))

        print('Box:', currBox[0:4], '\t\tScore:', currBox[4], '\tClass:', currBox[5])