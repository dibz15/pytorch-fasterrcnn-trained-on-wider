import torch

from PIL import Image
from utils import *

import cv2
import numpy as np

import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Face Detection using trained FasterRCNN model.')

#PyTorch Arguments
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--model_file', type=str, required=True, help="pre-trained model file")
parser.add_argument('--score_threshold', type=float, default=0.5, help="Threshold for box scoring.")
parser.add_argument('--iou_threshold', type=float, default=0.4, help="Threshold for box scoring.")

#OpenCV arguments
parser.add_argument('--model_input_size', type=float, default=512, help="Amount by which to downscale dataset images.")
parser.add_argument('--input_file', type=str, help="image to run model on")
parser.add_argument('--output_file', type=str, help='Output file to write final video or image if required')
parser.add_argument('--in_type', type=str, default='w', help='Input type. \'v\' for video, \'img\' for image, \'w\' for webcam.')
parser.add_argument('--out_scaled', action='store_true', help='Scale up output')
parser.add_argument('--noshow', action='store_true', help='Don\'t show output preview.')

#Parse Arguments
opt = parser.parse_args()

if not opt.input_file and not opt.in_type == 'w':
    print('If --in_type is not \'w\' then --input_file is required.')
    quit()
elif opt.input_file and opt.in_type == 'w':
    opt.in_type = 'img'

#Load and prepare model ====================
device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')
model = torch.load(opt.model_file, map_location=device)

if type(model) == dict:
    if 'full_model' in model.keys() and model['full_model'] is not None:
        model = model['full_model']
    elif 'model_state' in model.keys() and model['model_state'] is not None:
        if 'backbone_state' in model.keys():
            model = getModifiedFasterRCNN(backbone_state=model['backbone_state'], model_state=model['model_state'])
        else:
            model = getModifiedFasterRCNN(model_state=model['model_state'])


model = model.to(device)
model.eval()
#===========================================

#===========================================
#If source is image, open and predict
if opt.in_type == 'img':
    image = cv2.imread(opt.input_file)

    if image is None:
        print("Error opening file", opt.input_file, "as image.")
        exit(1)

    (oh, ow) = image.shape[:2]

    proportion = opt.model_input_size / max(oh, ow) 

    resizedImage = cv2.resize(image, (int(ow * proportion), int(oh * proportion)))

    modelInput = [getTestTransforms()(resizedImage).to(device)]
    pred = model(modelInput)
    sortedDict = sortPredictionsByClass(pred, 1)

    boxes = np.ndarray((0, 6))
    for k in sortedDict.keys():
        boxes = np.row_stack([boxes, nonMaximumSuppression_fast(sortedDict[k], confThresh=opt.score_threshold, iouThresh=opt.iou_threshold)])

    outputImg = image if opt.out_scaled else resizedImage
    outputImg = cv2.cvtColor(outputImg, cv2.COLOR_BGR2RGB)
    cvPutBoxes(outputImg, boxes, 1 / proportion if opt.out_scaled else 1.0)

    if not opt.noshow:
        plt.imshow(outputImg)
        plt.show()

    if opt.output_file:
        outputImg = cv2.cvtColor(outputImg, cv2.COLOR_BGR2RGB)
        cv2.imwrite(opt.output_file, outputImg)

elif opt.in_type == 'v' or opt.in_type == 'w':
    if opt.in_type == 'v':
        cap = cv2.VideoCapture(opt.input_file)
    else:
        cap = cv2.VideoCapture(0)
        print('Running with webcam capture. Press \'q\' to exit the app.')

    if not cap.isOpened():
        if opt.in_type == 'v':
            print("Error opening video file", opt.input_file)
        else:
            print("Error opening webcam stream.")
        exit(1)

    fps = int(cap.get(cv2.CAP_PROP_FPS)) if opt.in_type == 'v' else 24
    iw = int(cap.get(3))
    ih = int(cap.get(4))
    proportion = opt.model_input_size / max(ih, iw) 

    ow, oh =  int(iw * proportion), int(ih * proportion)

    vw = None
    if opt.output_file:
        vw = cv2.VideoWriter(opt.output_file, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), fps, (iw, ih) if opt.out_scaled else (ow, oh))

    while (cap.isOpened()):
        ret, frame = cap.read()
        if ret:
            resizedFrame = cv2.resize(frame, (ow, oh))

            modelInput = [getTestTransforms()(resizedFrame).to(device)]
            pred = model(modelInput)

            sortedDict = sortPredictionsByClass(pred, 1)

            boxes = np.ndarray((0, 6))
            for k in sortedDict.keys():
                foundBoxes = nonMaximumSuppression_fast(sortedDict[k], confThresh=opt.score_threshold, iouThresh=opt.iou_threshold)
                if len(foundBoxes) > 0:
                    boxes = np.row_stack([boxes, foundBoxes])

            # img = resizedFrame
            outputImg = frame if opt.out_scaled else resizedFrame
            cvPutBoxes(outputImg, boxes, 1 / proportion if opt.out_scaled else 1.0)

            if vw is not None:
                vw.write(outputImg)

            if not opt.noshow or opt.in_type == 'w':
                #Display the frame
                cv2.imshow('Frame', outputImg)
                
                #Press Q to exit video
                if cv2.waitKey(5) & 0xFF == ord('q'):
                    break

        else:
            break

    cap.release()
    
    if vw is not None:
        vw.release()

    cv2.destroyAllWindows()